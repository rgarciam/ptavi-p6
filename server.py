# Rebeca García Mencía
# !/usr/bin/python3
# -*- coding: utf-8 -*-

"""Clase (y programa principal) para un servidor de eco en UDP simple."""
import os
import socketserver
import sys


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    def system(self):
        """Se define el programa que envia en MP3."""
        aEjecutar = "mp32rtp -i " + IP + " " + "-p 23032" + " < " + FICHERO
        print("Vamos a ejecutar", aEjecutar)
        os.system(aEjecutar)

    def handle(self):
        """
        Handle method of the server class.

        (all requests will be handled by this method).
        """
        line = self.rfile.read()
        decodificar = line.decode('utf-8')
        print("El cliente nos manda ", decodificar)
        METHOD = decodificar.split(" ")[0]
        comprobar1 = decodificar.split(" ")[2]
        c = "SIP/2.0 100 Trying\r\n\r\n"
        c += "SIP/2.0 180 Ringing\r\n\r\n SIP/2.0 200 OK\r\n\r\n"
        if comprobar1 == "SIP/2.0\r\n\r\n":
            if METHOD == "INVITE":
                self.wfile.write(bytes(c, 'utf-8'))
            elif METHOD == "BYE":
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
            elif METHOD == "ACK":
                self.system()
            else:
                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
        else:
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    if len(sys.argv) != 4:
        sys.exit("Usage: server.py IP puerto audio_file")
    PORT = int(sys.argv[2])
    IP = sys.argv[1]
    FICHERO = sys.argv[3]
    serv = socketserver.UDPServer(('', PORT), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
