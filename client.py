# Rebeca García Mencía
# !/usr/bin/python3
# -*- coding: utf-8 -*-

"""Programa cliente que abre un socket a un servidor."""

import socket
import sys

if len(sys.argv) != 3:
    sys.exit("Usage: client.py method receiver@IP:SIPport")

LINEA = sys.argv[2]
METHOD = sys.argv[1]


if METHOD != "INVITE" and METHOD != "BYE":
    sys.exit("Usage: INVITE or BYE")

separador1 = LINEA.split("@")[1]
PORT = int(separador1.split(":")[-1])
SERVER = separador1.split(":")[0]
separador2 = LINEA.split(":")
ACCOUNT = separador2[0]
print(ACCOUNT)

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((SERVER, PORT))
    if str(sys.argv[1]) == "INVITE":
        MESSAGE = METHOD + " " + "sip:" + ACCOUNT + " " + "SIP/2.0" + "\r\n"
    if str(sys.argv[1]) == "BYE":
        MESSAGE = METHOD + " " + "sip:" + ACCOUNT + " " + "SIP/2.0" + "\r\n"
    print("Enviando: " + MESSAGE)
    my_socket.send(bytes(MESSAGE, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    DATOS = data.decode('utf-8')
    c = "SIP/2.0 100 Trying\r\n\r\n"
    c += "SIP/2.0 180 Ringing\r\n\r\n SIP/2.0 200 OK\r\n\r\n"
    print('Recibido -- ', data.decode('utf-8'))

    if DATOS == c:
        M_ACK = "ACK" + " " + "sip:" + ACCOUNT + " " + "SIP/2.0" + "\r\n"
        my_socket.send(bytes(M_ACK, 'utf-8') + b'\r\n')
        data = my_socket.recv(1024)
    print("Terminando socket...")
print("Fin.")
